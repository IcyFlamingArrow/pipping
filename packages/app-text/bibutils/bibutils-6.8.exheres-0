# Copyright 2009 Sterling X. Winter <replica@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PNV=${PN}_${PV}

require sourceforge flag-o-matic

DOWNLOADS="mirror://sourceforge/${PN}/${MY_PNV}_src.tgz"

SUMMARY="Bibliography conversion utilities"
DESCRIPTION="
The Bibutils program set interconverts between various bibliography formats
using a common MODS-format XML intermediate. For example, one can convert
RIS-format files to Bibtex by doing two transformations: RIS->MODS->Bibtex. By
using a common intermediate for N formats, only 2N programs are required and
not N²-N. These programs operate on the command line and are styled after
standard UNIX-like filters.
"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

WORK=${WORKBASE}/${MY_PNV}

# Makefile is non-standard and non-parallelizable
DEFAULT_SRC_COMPILE_PARAMS=( -j1 )

pkg_setup() {
    append-flags -fPIC
}

src_prepare() {
    edo cp Makefile{_start,}
    edo cp bin/Makefile{.dynamic,}
    edo cp lib/Makefile{.dynamic,}

    edo sed -f - -i Makefile <<EOF
s:REPLACE_CC:${CC}:
s:REPLACE_EXEEXT::
s:REPLACE_LIBTARGET:libbibutils.so:
s:REPLACE_LIBEXT:.so:
s:REPLACE_CFLAGS:${CFLAGS}:
s:REPLACE_CLIBFLAGS:${CFLAGS}:
s:REPLACE_RANLIB:${RANLIB}:
s:REPLACE_POSTFIX::
s:REPLACE_INSTALLDIR:${IMAGE}usr/$(exhost --target)/bin:
s:REPLACE_LIBINSTALLDIR:${IMAGE}usr/$(exhost --target)/lib:
EOF
}

# Do not use the configure script
src_configure() {
    :
}

src_install() {
    dodir /usr/$(exhost --target)/bin
    default
}

